//Haz un script que escriba una pirámide inversa de los números del 1 al número que indique el usuario (no mayor de 50)  de la siguiente forma : (suponiendo que indica 30).

let i = 0;
let x = 0;
let cantidad = 0;

cantidad = parseInt(prompt("Ingrese el numero maximo que tendra la piramide"));

if (cantidad > 50) {
    document.write("El maximo numero permitido es 50, por favor ingrese un numero menor...");
}
else
{

    for (i = cantidad; i > 0; i--) {
        for (x = cantidad; x > 0; x--) {
            document.write(i);
        }
        document.write("<br>");
    }
}