//10- Realiza un script que pida número de filas y columnas y escriba una tabla. Dentro cada una de las celdas deberá escribirse un número consecutivo en orden descendente. Si, por ejemplo, la tabla es de 7×5 los números irán del 35 al 1.

let i = 0;
let j = 0;
let cantidadFilas = 0;
let cantidadColumnas = 0;
let elementos = 0;
let numero = 0;

cantidadFilas = parseInt(prompt("Ingrese la cantidad de filas"));
cantidadColumnas = parseInt(prompt("Ingrese la cantidad de columnas"));
elementos = ((cantidadFilas * cantidadColumnas) + 1);
numero = (elementos - 1);

for (i = 0; i < cantidadFilas; i++) {
    document.write("<br>");
    for (j = 0; j < cantidadColumnas; j++) {
        document.write(numero);
        numero--;
    }
}