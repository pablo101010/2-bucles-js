//4- Realiza un script que pida números hasta que se pulse “cancelar”. Si no es un número deberá indicarse con un «alert» y seguir pidiendo números. Al salir con “cancelar” deberá indicarse la suma total de los números introducidos.


let numero=0;
let suma=0;
let parar;

do{
    numero=parseInt(prompt("Ingrese un numero"));
    if (isNaN(numero)) {
        alert("No es un numero"); 
      }
      else{
    suma = suma + numero;
      }
    parar=confirm("Desea seguir cargando numeros?");  
} while (parar==true);

document.write(suma);